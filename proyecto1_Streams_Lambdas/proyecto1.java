import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import java.util.List;
import java.util.TreeMap;

public class proyecto1 {
    public static void main(String[] args) throws IOException {
        Pattern pattern = Pattern.compile(",");

        List<Estudiante> estud = Files.lines(Paths.get("data.txt")).map(line -> {
            Estudiante e = new Estudiante();
            String[] data = pattern.split(line);
            e.setCode(data[0]);
            e.setFirstName(data[1]);
            e.setLastName(data[2]);
            e.setBirthday(data[3]);
            e.setCreditsCount(Integer.parseInt(data[4]));
            Pattern.compile("-").splitAsStream(data[5]).forEach(code -> e.addCourse(code));
            return e;
        }).collect(Collectors.toList());

        // Impresion lista ordenada por codigo con cursos ordenados
        System.out.println("Lista ordenada por código con cursos");
        estud.stream().sorted().forEach(e -> {
            System.out.println(e + " \n Cursos en orden: "
                    + e.getCoursesListAsStream().sorted().collect(Collectors.joining(", ")));
        });

        // Impresion de lista por apellidos y con cursos ordenados inverso alfabetico
        System.out.println("\nLista ordenada por apellido con cursos en orden inverso");
        estud.stream().sorted((x1, x2) -> x1.getLasName().compareTo(x2.getLasName())).forEach(e -> {
            System.out.println(e + "\n Cursos orden inverso: " + e.getCoursesListAsStream()
                    .sorted((x1, x2) -> x1.compareTo(x2) * (-1)).collect(Collectors.joining(", ")));
        });

        // Imprimir los estudiantes ordenados y agrupados por el mismo numero de
        // créditos.
        System.out.println("\nLista ordenada y agrupado por el mismo número de creditos");
        estud.stream().collect(Collectors.groupingBy(Estudiante::getCreditsCount, TreeMap::new, Collectors.toList()))
                .forEach((key, list) -> {
                    System.out.println(key);
                    list.stream().sorted().forEach(System.out::println);
                });

        // Imprimir los estudiantes ordenado y
        // agrupados por año de nacimiento.
        System.out.println("\nLista ordenanda por año de nacimiento");
        estud.stream().collect(Collectors.groupingBy(e -> e.getBirthday().getYear(), TreeMap::new, Collectors.toList()))
                .forEach((year, list) -> {
                    System.out.println(year);
                    System.out.println(list.toString());
                });
        // Imprimir los estudiantes que tienen 2
        // ó mas cursos de CMP, ordenados por código y la
        // lista de cursos CMP completa.
        System.out.println("\nLista de estudiantes con dos o mas cursos CMP");
        estud.stream().filter(e -> e.getCoursesListAsStream().filter(c -> c.matches("CMP[0-9]{3}")).count() >= 2)
                .sorted().forEach(e -> {
                    System.out.println(e);
                    System.out.println(e.getCoursesListAsStream().filter(c -> c.matches("CMP[0-9]{3}")).sorted()
                            .collect(Collectors.joining(", ")));
                });

        // Imprimir los estudiantes que no
        // tienen cursos de CMP, ordenados por apellido y
        // la lista de cursos completa
        System.out.println("\nLista de estudiantes sin cursos CMP");
        estud.stream().filter(e -> e.getCoursesListAsStream().filter(c -> c.matches("CMP[0-9]{3}")).count() <= 0)
                .sorted((x1, x2) -> x1.getLasName().compareTo(x2.getLasName())).forEach(e -> {
                    System.out.println(e);
                    System.out.println(e.getCoursesListAsStream().sorted().collect(Collectors.joining(", ")));
                });
    }
}
