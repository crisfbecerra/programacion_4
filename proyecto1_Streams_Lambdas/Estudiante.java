import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.security.InvalidParameterException;

/**
 * Clase que define a un estudiante dentro del proyecto
 * 
 * @author Cristopher Becerra
 * @version 28-01-2021
 */

public class Estudiante implements Comparable<Estudiante> {
    // Parametros
    private String code;
    private String firstName;
    private String lasName;
    private LocalDate birthday;
    private Set<String> coursesList = new HashSet<String>();
    private int creditsCount = 0;

    private static Pattern ISODatePatter = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
    private static Pattern CourseCodeFormat = Pattern.compile("[A-Z]{3}[0-9]{3}");

    public Estudiante() {

    }

    // Mutadores
    /**
     * Metodo para mutar code
     * 
     * @param code
     * @return this
     */
    public Estudiante setCode(String code) {
        // if (!code.matches("[0-9]{8}")) {
        // throw new InvalidAttributeValueException("Invalid code formate -> " + code);
        // }
        this.code = code;
        return this;
    }

    /**
     * Methodo de mutación de primer nombre
     * 
     * @param firstName
     * @return this
     */
    public Estudiante setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * Metodo de mutación de apellido
     * 
     * @param lastName
     * @return this
     */
    public Estudiante setLastName(String lastName) {
        this.lasName = lastName;
        return this;
    }

    /**
     * Método de mutación de fecha de nacimiento
     * 
     * @param birthdayOnISOString
     * @return this
     * @throws InvalidAttributeValueException
     */
    public Estudiante setBirthday(String birthdayOnISOString) throws InvalidParameterException {

        if (!ISODatePatter.matcher(birthdayOnISOString).matches()) {
            throw new InvalidParameterException("birthdayISOString must be a ISO valid date");
        }
        this.birthday = LocalDate.parse(birthdayOnISOString);
        return this;
    }

    /**
     * Agrear un curso a la lista de cursos del estudiante
     * 
     * @param courseCode
     * @return this
     */
    public Estudiante addCourse(String courseCode) {
        if (!CourseCodeFormat.matcher(courseCode).matches()) {
            throw new InvalidParameterException("Invalid course code");
        }
        this.coursesList.add(courseCode);
        return this;
    }

    /**
     * Método para mutar el número de créditos
     * 
     * @param creditsCount
     * @return this
     */
    public Estudiante setCreditsCount(int creditsCount) {
        this.creditsCount = creditsCount;
        return this;
    }

    // ACCESORES
    public String getCode() {
        return this.code;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLasName() {
        return this.lasName;
    }

    public String getFullName() {
        return this.firstName + " " + this.lasName;
    }

    public String getBirthdayAsString() {
        return this.birthday.toString();
    }

    public LocalDate getBirthday() {
        return this.birthday;
    }

    public int getCreditsCount() {
        return this.creditsCount;
    }

    public Stream<String> getCoursesListAsStream() {
        return this.coursesList.stream();
    }

    // ToString
    @Override
    public String toString() {
        return String.format(
                "{Codigo: %s ,Nombre: %s ,Apellido: %s ,Fecha de naciemiento: %s , Cantidad de creditos: %d }",
                this.code, this.firstName, this.lasName, this.birthday.toString(), this.creditsCount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Estudiante))
            return false;
        return this.code.equals(((Estudiante) o).getCode());
    }

    @Override
    public int compareTo(Estudiante e) {
        return this.code.compareTo(e.getCode());
    }

    public String getCoursesList() {
        return this.coursesList.stream().collect(Collectors.joining(","));
    }
}
